#!/usr/bin/env python3
import tkinter as tk
from tkinter import messagebox
import tkinter.ttk as ttk
import os
from .StartPage import StartPageFrame
from .SearchWordsPage import SearchWordsPageFrame
from PIL import Image, ImageTk
import traceback


__title__ = "Dualisbot v1.0"
font16 = "-family Calibri -size 12 -weight normal -slant roman"  \
    " -underline 0 -overstrike 0"
font17 = "-family Calibri -size 24 -weight normal -slant roman"  \
    " -underline 0 -overstrike 0"
font18 = "-family Calibri -size 15 -weight normal -slant roman"  \
    " -underline 0 -overstrike 0"
font22 = "-family Calibri -size 8 -weight normal -slant roman "  \
    "-underline 0 -overstrike 0"
font9 = "-family Calibri -size 12 -weight normal -slant roman "  \
    "-underline 0 -overstrike 0"

class GUI(tk.Tk):
    def __init__(self, *args, **kwargs):

        tk.Tk.__init__(self, *args, **kwargs)

        self.geometry("605x454+315+130")
        self.title("Dualisbot")
        self.configure(borderwidth="3")
        self.configure(relief="groove")
        self.configure(background="#ffffff")
        self.configure(highlightbackground="#eaeaea")
        self.configure(highlightcolor="black")

        container = tk.Frame(self)

        container.pack(side="top", fill="both", expand=True)

        container.grid_rowconfigure(0, weight=1)
        container.grid_columnconfigure(0, weight=1)

        self.frames = {}

        for F in (StartPageFrame, SearchWordsPageFrame):
            frame = F(container, self)
            self.frames[F] = frame
            frame.grid(row=0, column=0, sticky="nsew")

        self.show_frame(StartPageFrame)

    def show_frame(self, cont):
        frame = self.frames[cont]
        frame.tkraise()

    def new_window(self):
        window = tk.Toplevel(self)
        window.geometry("400x300+315+130")
        window.title("Dualisbot")
        window.configure(borderwidth="3")
        window.configure(relief="groove")
        window.configure(background="#ffffff")
        window.configure(highlightbackground="#eaeaea")
        window.configure(highlightcolor="black")
        label = tk.Label(window, text="Hallo")
        label.pack(side="top", fill="both", padx=10, pady=10)

def start_gui():
    app = GUI()
    app.title(__title__)

    app.mainloop()