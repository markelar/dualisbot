#!/usr/bin/env python3
import tkinter as tk
import tkinter.ttk as ttk
import os
from ..config import Config
from ..helpers import Helpers


from PIL import Image, ImageTk

__title__ = "Dualisbot v1.0"
font16 = "-family Calibri -size 12 -weight normal -slant roman"  \
    " -underline 0 -overstrike 0"
font17 = "-family Calibri -size 24 -weight normal -slant roman"  \
    " -underline 0 -overstrike 0"
font18 = "-family Calibri -size 15 -weight normal -slant roman"  \
    " -underline 0 -overstrike 0"
font22 = "-family Calibri -size 8 -weight normal -slant roman "  \
    "-underline 0 -overstrike 0"
font9 = "-family Calibri -size 12 -weight normal -slant roman "  \
    "-underline 0 -overstrike 0"

class SearchWordsPageFrame(tk.Frame):
    def __init__(self, parent, controller):
        from .StartPage import StartPageFrame
        tk.Frame.__init__(self, parent)
        self.configure(background="#ffffff")

        self.LabelMainMenuTitle = tk.Label(self)
        self.LabelMainMenuTitle.place(relx=0.033, rely=0.121, height=55
                , width=148)
        self.LabelMainMenuTitle.configure(activebackground="#f9f9f9")
        self.LabelMainMenuTitle.configure(activeforeground="black")
        self.LabelMainMenuTitle.configure(background="#ffffff")
        self.LabelMainMenuTitle.configure(disabledforeground="#a3a3a3")
        self.LabelMainMenuTitle.configure(font=font17)
        self.LabelMainMenuTitle.configure(foreground="#000000")
        self.LabelMainMenuTitle.configure(highlightbackground="#eaeaea")
        self.LabelMainMenuTitle.configure(highlightcolor="black")
        self.LabelMainMenuTitle.configure(padx="0")
        self.LabelMainMenuTitle.configure(pady="0")
        self.LabelMainMenuTitle.configure(text='''Dualisbot''')

        self.LabelDHBWLogo = tk.Label(self)
        self.LabelDHBWLogo.place(relx=0.752, rely=0.044, height=125, width=125)
        self.LabelDHBWLogo.configure(background="#ffffff")
        self.LabelDHBWLogo.configure(cursor="fleur")
        self.LabelDHBWLogo.configure(disabledforeground="#a3a3a3")
        self.LabelDHBWLogo.configure(font=font16)
        self.LabelDHBWLogo.configure(foreground="#000000")
        self.LabelDHBWLogo.configure(cursor="arrow")
        self.image_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), "dhbwlogo.png")
        self.image = Image.open(self.image_path)
        self.photo = ImageTk.PhotoImage(self.image)
        self.LabelDHBWLogo.configure(image=self.photo)
        self.LabelDHBWLogo.configure(text='''Label''')

        self.LabelVersion = tk.Label(self)
        self.LabelVersion.place(relx=0.694, rely=0.923, height=25, width=151)
        self.LabelVersion.configure(activebackground="#f9f9f9")
        self.LabelVersion.configure(activeforeground="black")
        self.LabelVersion.configure(background="#ffffff")
        self.LabelVersion.configure(disabledforeground="#a3a3a3")
        self.LabelVersion.configure(font=font22)
        self.LabelVersion.configure(foreground="#000000")
        self.LabelVersion.configure(highlightbackground="#eaeaea")
        self.LabelVersion.configure(highlightcolor="black")
        self.LabelVersion.configure(text=Config.VERSION)

        self.TextSearchTerms = tk.Text(self)
        self.TextSearchTerms.place(relx=0.05, rely=0.374, relheight=0.536, relwidth=0.684)
        self.TextSearchTerms.configure(background="white")
        self.TextSearchTerms.configure(font=font9)
        self.TextSearchTerms.configure(foreground="black")
        self.TextSearchTerms.configure(highlightbackground="#eaeaea")
        self.TextSearchTerms.configure(highlightcolor="black")
        self.TextSearchTerms.configure(insertbackground="black")
        self.TextSearchTerms.configure(selectbackground="#c4c4c4")
        self.TextSearchTerms.configure(selectforeground="black")
        self.TextSearchTerms.configure(width=414)
        self.TextSearchTerms.configure(wrap="word")
        search_terms = Helpers.get_search_terms(Helpers, split=False)
        self.TextSearchTerms.insert(tk.END, search_terms)

        self.ButtonSave = tk.Button(self)
        self.ButtonSave.place(relx=0.793, rely=0.462, height=31, width=85)
        self.ButtonSave.configure(activebackground="#ececec")
        self.ButtonSave.configure(activeforeground="#000000")
        self.ButtonSave.configure(background="#eaeaea")
        self.ButtonSave.configure(disabledforeground="#a3a3a3")
        self.ButtonSave.configure(font=font9)
        self.ButtonSave.configure(foreground="#000000")
        self.ButtonSave.configure(highlightbackground="#eaeaea")
        self.ButtonSave.configure(highlightcolor="black")
        self.ButtonSave.configure(pady="0")
        self.ButtonSave.configure(text='''Speichern''')
        self.ButtonSave.configure(width=85)
        self.ButtonSave.configure(command=lambda: self.save_search_terms(controller, StartPageFrame))

        self.ButtonBack = tk.Button(self)
        self.ButtonBack.place(relx=0.793, rely=0.571, height=31, width=85)
        self.ButtonBack.configure(activebackground="#ececec")
        self.ButtonBack.configure(activeforeground="#000000")
        self.ButtonBack.configure(background="#eaeaea")
        self.ButtonBack.configure(disabledforeground="#a3a3a3")
        self.ButtonBack.configure(font=font9)
        self.ButtonBack.configure(foreground="#000000")
        self.ButtonBack.configure(highlightbackground="#eaeaea")
        self.ButtonBack.configure(highlightcolor="black")
        self.ButtonBack.configure(pady="0")
        self.ButtonBack.configure(text='''Zurück''')
        self.ButtonBack.configure(width=85)
        self.ButtonBack.configure(command=lambda: controller.show_frame(StartPageFrame))
    
    def save_search_terms(self, controller, StartPageFrame):
        input_words = self.TextSearchTerms.get("1.0", tk.END)
        st_list = input_words.split()
        input_words = "\n".join(st_list)
        try:
            with open(Config.SEARCH_TERMS_STORAGE_PATH, 'w', encoding="cp1252") as f:
                f.write(input_words)
        except Exception as e:
            print(e)
            pass
        finally: controller.show_frame(StartPageFrame)
            
