#!/usr/bin/env python3
import tkinter as tk
import tkinter.ttk as ttk
from tkinter import filedialog
from tkinter import *
import os
from .SearchWordsPage import SearchWordsPageFrame
from ..config import Config
from run_bot import *
from .ToolTip import *
from PIL import Image, ImageTk

__title__ = "Dualisbot v1.0"
font15 = "-family Calibri -size 12 -weight bold -slant roman"  \
    " -underline 0 -overstrike 0"
font16 = "-family Calibri -size 12 -weight normal -slant roman"  \
    " -underline 0 -overstrike 0"
font17 = "-family Calibri -size 24 -weight normal -slant roman"  \
    " -underline 0 -overstrike 0"
font18 = "-family Calibri -size 15 -weight normal -slant roman"  \
    " -underline 0 -overstrike 0"
font19 = "-family Calibri -size 10 -weight normal -slant roman"  \
    " -underline 0 -overstrike 0"
font22 = "-family Calibri -size 8 -weight normal -slant roman "  \
    "-underline 0 -overstrike 0"
font9 = "-family Calibri -size 12 -weight normal -slant roman "  \
    "-underline 0 -overstrike 0"

class StartPageFrame(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.configure(background="#ffffff")

        self.LabelMainMenuTitle = tk.Label(self)
        self.LabelMainMenuTitle.place(relx=0.033, rely=0.121, height=55
                , width=148)
        self.LabelMainMenuTitle.configure(background="#ffffff")
        self.LabelMainMenuTitle.configure(disabledforeground="#a3a3a3")
        self.LabelMainMenuTitle.configure(font=font17)
        self.LabelMainMenuTitle.configure(foreground="#000000")
        self.LabelMainMenuTitle.configure(text='''Dualisbot''')

        self.ButtonStartBot = tk.Button(self)
        self.ButtonStartBot.place(relx=0.05, rely=0.484, height=61, width=135)
        self.ButtonStartBot.configure(activebackground="#ececec")
        self.ButtonStartBot.configure(activeforeground="#000000")
        self.ButtonStartBot.configure(background="#eaeaea")
        self.ButtonStartBot.configure(disabledforeground="#a3a3a3")
        self.ButtonStartBot.configure(font=font15)
        self.ButtonStartBot.configure(foreground="#000000")
        self.ButtonStartBot.configure(highlightbackground="#eaeaea")
        self.ButtonStartBot.configure(highlightcolor="black")
        self.ButtonStartBot.configure(pady="0")
        self.ButtonStartBot.configure(text='''Bot starten''')
        self.ButtonStartBot.configure(command=lambda: run_bot())


        self.ButtonSearchWords = tk.Button(self)
        self.ButtonSearchWords.place(relx=0.05, rely=0.67, height=31, width=135)
        self.ButtonSearchWords.configure(activebackground="#ececec")
        self.ButtonSearchWords.configure(activeforeground="#000000")
        self.ButtonSearchWords.configure(background="#eaeaea")
        self.ButtonSearchWords.configure(disabledforeground="#a3a3a3")
        self.ButtonSearchWords.configure(font=font16)
        self.ButtonSearchWords.configure(foreground="#000000")
        self.ButtonSearchWords.configure(highlightbackground="#eaeaea")
        self.ButtonSearchWords.configure(highlightcolor="black")
        self.ButtonSearchWords.configure(pady="0")
        self.ButtonSearchWords.configure(text='''Suchwörter''')
        self.ButtonSearchWords.configure(command=lambda: controller.show_frame(SearchWordsPageFrame))

        self.LabelDHBWLogo = tk.Label(self)
        self.LabelDHBWLogo.place(relx=0.752, rely=0.044, height=125, width=125)
        self.LabelDHBWLogo.configure(background="#ffffff")
        self.LabelDHBWLogo.configure(cursor="fleur")
        self.LabelDHBWLogo.configure(disabledforeground="#a3a3a3")
        self.LabelDHBWLogo.configure(font=font16)
        self.LabelDHBWLogo.configure(foreground="#000000")
        self.LabelDHBWLogo.configure(cursor="arrow")
        self.image_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), "dhbwlogo.png")
        self.image = Image.open(self.image_path)
        self.photo = ImageTk.PhotoImage(self.image)
        self.LabelDHBWLogo.configure(image=self.photo)
        self.LabelDHBWLogo.configure(text='''Label''')

        self.LabelInputLocation = tk.Label(self)
        self.LabelInputLocation.place(relx=0.545, rely=0.505, height=45, width=181)
        self.LabelInputLocation.configure(background="#ffffff")
        self.LabelInputLocation.configure(disabledforeground="#a3a3a3")
        self.LabelInputLocation.configure(font=font19)
        self.LabelInputLocation.configure(foreground="#000000")
        input_location = "----------"
        if Config.INPUT_STORAGE_PATH:
            input_location = Config.INPUT_STORAGE_PATH
            createToolTip(self.LabelInputLocation, input_location)
        self.LabelInputLocation.configure(anchor="w")
        self.LabelInputLocation.configure(text=input_location)
        self.LabelInputLocation.configure(width=181)

        self.LabelOutputLocation = tk.Label(self)
        self.LabelOutputLocation.place(relx=0.545, rely=0.659, height=45, width=181)
        self.LabelOutputLocation.configure(background="#ffffff")
        self.LabelOutputLocation.configure(disabledforeground="#a3a3a3")
        self.LabelOutputLocation.configure(font=font19)
        self.LabelOutputLocation.configure(foreground="#000000")
        self.LabelOutputLocation.configure(text=Config.OUTPUT_STORAGE_PATH)
        self.LabelOutputLocation.configure(width=181)
        self.LabelOutputLocation.configure(anchor="w")
        createToolTip(self.LabelOutputLocation, Config.OUTPUT_STORAGE_PATH)

        self.ButtonChangeInput = tk.Button(self)
        self.ButtonChangeInput.place(relx=0.86, rely=0.516, height=31, width=55)
        self.ButtonChangeInput.configure(activebackground="#ececec")
        self.ButtonChangeInput.configure(activeforeground="#000000")
        self.ButtonChangeInput.configure(background="#eaeaea")
        self.ButtonChangeInput.configure(disabledforeground="#a3a3a3")
        self.ButtonChangeInput.configure(font=font16)
        self.ButtonChangeInput.configure(foreground="#000000")
        self.ButtonChangeInput.configure(highlightbackground="#eaeaea")
        self.ButtonChangeInput.configure(highlightcolor="black")
        self.ButtonChangeInput.configure(pady="0")
        self.ButtonChangeInput.configure(text='''Ändern''')
        self.ButtonChangeInput.configure(command=lambda: self.select_input_file())

        self.LabelLocation = tk.Label(self)
        self.LabelLocation.place(relx=0.314, rely=0.374, height=30, width=132)
        self.LabelLocation.configure(background="#ffffff")
        self.LabelLocation.configure(disabledforeground="#a3a3a3")
        self.LabelLocation.configure(font=font18)
        self.LabelLocation.configure(foreground="#000000")
        self.LabelLocation.configure(text='''Speicherorte''')
        self.LabelLocation.configure(width=132)

        self.LabelInputCSV = tk.Label(self)
        self.LabelInputCSV.place(relx=0.347, rely=0.527, height=25, width=91)
        self.LabelInputCSV.configure(background="#ffffff")
        self.LabelInputCSV.configure(disabledforeground="#a3a3a3")
        self.LabelInputCSV.configure(font=font16)
        self.LabelInputCSV.configure(foreground="#000000")
        self.LabelInputCSV.configure(text='''Input-CSV''')
        self.LabelInputCSV.configure(width=91)

        self.LabelOutputCSV = tk.Label(self)
        self.LabelOutputCSV.place(relx=0.347, rely=0.681, height=25, width=91)
        self.LabelOutputCSV.configure(background="#ffffff")
        self.LabelOutputCSV.configure(disabledforeground="#a3a3a3")
        self.LabelOutputCSV.configure(font=font16)
        self.LabelOutputCSV.configure(foreground="#000000")
        self.LabelOutputCSV.configure(text='''Output-CSV''')
        self.LabelOutputCSV.configure(width=91)

        self.ButtonChangeOutput = tk.Button(self)
        self.ButtonChangeOutput.place(relx=0.86, rely=0.67, height=31, width=55)
        self.ButtonChangeOutput.configure(activebackground="#ececec")
        self.ButtonChangeOutput.configure(activeforeground="#000000")
        self.ButtonChangeOutput.configure(background="#eaeaea")
        self.ButtonChangeOutput.configure(disabledforeground="#a3a3a3")
        self.ButtonChangeOutput.configure(font=font16)
        self.ButtonChangeOutput.configure(foreground="#000000")
        self.ButtonChangeOutput.configure(highlightbackground="#eaeaea")
        self.ButtonChangeOutput.configure(highlightcolor="black")
        self.ButtonChangeOutput.configure(pady="0")
        self.ButtonChangeOutput.configure(text='''Ändern''')
        self.ButtonChangeOutput.configure(command=lambda: self.select_output_folder())

        self.TSeparator1 = ttk.Separator(self)
        self.TSeparator1.place(relx=0.306, rely=0.374, relheight=0.484)
        self.TSeparator1.configure(orient="vertical")

        self.LabelVersion = tk.Label(self)
        self.LabelVersion.place(relx=0.694, rely=0.923, height=25, width=151)
        self.LabelVersion.configure(background="#ffffff")
        self.LabelVersion.configure(disabledforeground="#a3a3a3")
        self.LabelVersion.configure(font=font22)
        self.LabelVersion.configure(foreground="#000000")
        self.LabelVersion.configure(text=Config.VERSION)
        self.LabelVersion.configure(width=151)

        # self.Progressbar1 = ttk.Progressbar(self)
        # self.Progressbar1.place(relx=0.364, rely=0.813, relwidth=0.579
        #         , relheight=0.0, height=22)
        # self.Progressbar1.configure(length="360")
        # self.Progressbar1.configure(mode="indeterminate")
        # self.Progressbar1.start()

        # self.CheckButtonAdminMode = tk.Checkbutton(self)
        # self.CheckButtonAdminMode.place(relx=0.058, rely=0.769, relheight=0.066
        #         , relwidth=0.212)
        # self.CheckButtonAdminMode.configure(activebackground="#ffffff")
        # self.CheckButtonAdminMode.configure(activeforeground="#000000")
        # self.CheckButtonAdminMode.configure(background="#ffffff")
        # self.CheckButtonAdminMode.configure(disabledforeground="#a3a3a3")
        # self.CheckButtonAdminMode.configure(font=font9)
        # self.CheckButtonAdminMode.configure(foreground="#000000")
        # self.CheckButtonAdminMode.configure(highlightbackground="#ffffff")
        # self.CheckButtonAdminMode.configure(highlightcolor="black")
        # self.CheckButtonAdminMode.configure(justify='left')
        # self.CheckButtonAdminMode.configure(text='''Admin-Modus''')
        # # self.CheckButtonAdminMode.configure(variable=mainmenu_support.che45)
        # self.CheckButtonAdminMode.configure(width=128)
    
    def select_input_file(self):
        file = tk.filedialog.askopenfilename(initialdir="C:\\", title="Datei auswählen", filetypes = [("CSV-Dateien","*.csv")])
        if file != None:
            file = file.replace("/", "\\")
            Config.INPUT_STORAGE_PATH = file
            self.LabelInputLocation.configure(text=Config.INPUT_STORAGE_PATH)
    
    def select_output_folder(self):
        folder = tk.filedialog.askdirectory(initialdir="C:\\", title="Ordner auswählen")
        if folder != None:
            folder = folder.replace("/", "\\")
            Config.OUTPUT_STORAGE_PATH = folder
            self.LabelOutputLocation.configure(text=Config.OUTPUT_STORAGE_PATH)
    
    def stop_progressbar(self):
        self.Progressbar1.stop()
