from os.path import abspath, join, dirname, isdir, realpath
from pathlib import Path
import pysnooper
import pickle

class Config():
    UA_STORAGE_PATH = None
    INPUT_STORAGE_PATH = None
    OUTPUT_STORAGE_PATH = None
    SEARCH_TERMS_STORAGE_PATH = realpath("./ceo_searchterms.txt")
    VERSION = "Dualisbot v1.0"
