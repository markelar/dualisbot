import scrapy
import bs4
import re
from ..helpers import Helpers
# from selenium import webdriver
import time
import pysnooper
from os.path import join
import json
import sys
from dualisbot.spiders.ceo_result import CEO_result

class CEOSpider(scrapy.Spider):
    name = "CEO"

    def __init__(self, domain, ID):
        self.url = domain
        self.ID = ID

    def start_requests(self):
        if self.url.endswith('/'):
            self.url = self.url[:-1]
        yield scrapy.Request(url=self.url, callback=self.parse)

    def parse(self, response):
        body = response.body
        soup = bs4.BeautifulSoup(body, "lxml")
        h = Helpers()
        url_impressum = h.get_impressum_url(soup)
        if not url_impressum:
            pattern = re.compile(r"https://www\..*(\.[a-z]+)")
            match = re.match(pattern, self.url)
            domain = match.group(1)
            if domain is not ".de":
                self.url = self.url.replace(domain, ".de")
                return scrapy.Request(url=self.url, callback=self.parse_re)
        else:
            if not url_impressum.startswith(("www", "http")):
                url_impressum = response.urljoin(url_impressum)
            meta = {'dont_redirect': True,'handle_httpstatus_list': [301, 302]}
            request = scrapy.Request(url=url_impressum, meta=meta, callback=self.parse2)
            return request

    def parse_re(self, response):
        body = response.body
        soup = bs4.BeautifulSoup(body, "lxml")
        h = Helpers()
        url_impressum = h.get_impressum_url(soup)
        if url_impressum:
            if not url_impressum.startswith(("www", "http")):
                url_impressum = response.urljoin(url_impressum)
            meta = {'dont_redirect': True,'handle_httpstatus_list': [301, 302]}
            request = scrapy.Request(url=url_impressum, meta=meta, callback=self.parse2)
            return request

    def parse2(self, response):
        body = response.body
        soup = bs4.BeautifulSoup(body, "lxml")

        h = Helpers()
        ceo = h.get_CEO3(soup)
        h.save_ceo2(ceo, self.ID)

