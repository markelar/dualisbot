import scrapy
from ..helpers import Helpers
import time
import pysnooper
from ..config import Config
from pathlib import Path
import json


class UserAgentSpider(scrapy.Spider):
    name = "UserAgent"

    def __init__(self, domain):
        self.domain = domain

    def start_requests(self):
        yield scrapy.Request(url=self.domain, callback=self.parse)

    def parse(self, response):
        uas = response.css('textarea.get-the-list:nth-of-type(2)::text').get()
        print("-----------s")
        print(uas)
        uas_json = json.loads(uas)
        Helpers.save_user_agents(Helpers, uas_json)
        
        # try:
        #     path = Path(Config.UA_STORAGE_PATH, 'useragents.txt')
        #     with open(path, 'w') as f:
        #         for i in range(15):
        #             f.write("%s\n" % uas_json[i]['useragent']) #todo: windows hat \r\n
        # except Exception:
        #     pass