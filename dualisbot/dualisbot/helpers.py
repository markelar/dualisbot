import bs4
import re
import pysnooper
from pathlib import Path
import time
from dualisbot.config import Config
from dualisbot.spiders.ceo_result import CEO_result
from os.path import (join, exists, isdir, getmtime)
import os
import random
import csv
import sys

class Helpers(object):
    def is_valid_path(self, path, errortext):
        try:
            if isdir(path):
                return True
            open(path, 'r')
            return True
        except Exception:
            from dualisbot.gui.GUI import messagebox
            messagebox.showinfo("Fehler", errortext)
            return False

    def csv_to_dict(self):
        try:
            from dualisbot.gui.GUI import messagebox
            r = list(csv.reader(open(Config.INPUT_STORAGE_PATH), delimiter=';'))
        except:
            messagebox.showinfo("Fehler", "Fehler beim Einlesen der CSV-Datei")
            return None
        d = {}
        i = 0
        headers = []
        for row in r:
            d[i] = {}
            if row == r[0]:
                for header in row:
                    d[i][header] = {}
                    if header not in CEO_result.CSV_HEADERS:
                        CEO_result.CSV_HEADERS.append(header)
                CEO_result.CSV_HEADERS.append("Dualisbot")
                continue
            for index, value in enumerate(row):
                d[i][CEO_result.CSV_HEADERS[index]] = value
            d[i]["Dualisbot"] = ""
            i += 1
        CEO_result.CSV_DICT = d
        return d
    
    def get_companies_url(self, csvdict: dict):
        if not csvdict:
            return None
        urls = {}
        i = 0
        for i in range(len(csvdict)):
            try:
                ID = csvdict[i]['CampusNet_ID']
                pattern = re.compile(r'.*@(.*\.[a-z]*)')
                url = re.match(pattern, csvdict[i]['Email']).group(1)
                httpsurl = "https://www." + url
                urls[ID] = httpsurl
            except:
                pass
        return urls

    def get_impressum_url(self, soup: bs4.BeautifulSoup):
        a_tags = soup.find_all('a')
        pattern = re.compile(r'.*Impressum.*', re.I)
        for a_tag in a_tags:
            if pattern.search(a_tag.get_text()):
                link = a_tag['href']
                return link
        return ""     

    def get_search_terms(self, split=True):
        try:
            filepath = Path(Config.SEARCH_TERMS_STORAGE_PATH)

            with open(filepath, 'r', encoding="cp1252") as f:
                sts = f.read()
                if split:
                    sts = sts.splitlines()

            return sts

        except Exception as e:
            print(e)
            pass

    def get_CEO3(self, soup: bs4.BeautifulSoup):
        all_tags = soup.find_all()
        searchterms = self.get_search_terms()
        ceo = []
        for searchterm in searchterms:
            pattern = re.compile(r'([a-zA-Z-\/\säöüß0-9\.]*' + searchterm + '[a-zA-Z-\/\säöüß]*:*)', re.I)
            pattern2 = re.compile(r'([a-zA-Z-\/\säöüß]*' + searchterm + '[a-zA-Z-\/\säöüß]*)[:-]\s*(\S.*)', re.I)
            
            found = False
            title = ""
            for tag in all_tags:
                if tag.has_attr('href') or str(tag) in ["<br>", "<br/>"]:
                    continue

                if found:
                    if tag.get_text() == title:
                        continue
                    ceo.append(tag.get_text())
                    found = False
                    break

                contents = tag.contents
                nextline = False
                for content in contents:

                    match = re.match(pattern, str(content))
                    
                    if match:
                        found = False
                        if str(tag).startswith("<strong>") or str(tag).startswith("<b>"):

                            sibling = tag.next_sibling

                            if sibling and str(sibling):

                                con = False
                                found_none = True
                                while True:
                                    if not sibling:
                                        con = True
                                        break

                                    if not str(sibling).isspace() and str(sibling) not in ["<br>", "<br/>"]:
                                        ceo.append(sibling)
                                        found_none = False

                                    sibling = sibling.next_sibling

                                if con:
                                    if found_none:
                                        nextline = True
                                    continue
                            else:
                                nextline = True
                                continue
                        title = match.group(1).rstrip()
                        if type(content) is bs4.element.NavigableString:
                            c = str(content)
                        if type(content) is bs4.element.Tag:
                            if str(content) in ["<br>", "<br/>"]:
                                continue
                            c = content.get_text()
                        if not nextline:
                            match2 = re.match(pattern, c)
                            match3 = re.match(pattern2, c)
                            if match2 and not match3:
                                nextline = True
                                continue
                            if match2 and match3:
                                nextline = False
                                ceo.append(match3.group(2))
                        if nextline:
                            if str(content) and not str(content).startswith("<") and not str(content).isspace():
                                nextline = False
                                ceo.append(str(content))
                if nextline:
                    found = True
                    continue

                found = False
            
        return ceo

    def save_ceo2(self, ceo_list: list, ID: int):
        CEO_result.ALL_CEOS_DICT[ID] = ceo_list

    def write_ceo_csv(self):
        all_ceos_dict = CEO_result.ALL_CEOS_DICT
        csv_dict = CEO_result.CSV_DICT
        file_name = str(time.strftime("%Y")) + "-" + str(time.strftime("%m")) + "-" + str(time.strftime("%d")) + "ceo.csv"
        file_path = Path(Config.OUTPUT_STORAGE_PATH, file_name)
        headers = CEO_result.CSV_HEADERS    
        for ID in all_ceos_dict:
            j = 0
            for i in range(len(csv_dict)):
                try:
                    if csv_dict[i]['CampusNet_ID'] == str(ID):
                        j = i
                        old_ceo = csv_dict[i]['Geschäftsführer'].rstrip()
                        pattern = re.compile(r'' + old_ceo + '', re.I)
                        for new_ceo in all_ceos_dict[ID]:
                            if not re.match(pattern, new_ceo) or not old_ceo or old_ceo.isspace():
                                csv_dict[i]['Geschäftsführer'] = new_ceo
                                csv_dict[i]['Dualisbot'] = 1
                                all_ceos_dict[ID].remove(new_ceo)
                                break

                            if re.match(pattern, new_ceo):
                                all_ceos_dict[ID].remove(new_ceo)
                                break
                except KeyError:
                    from dualisbot.gui.GUI import messagebox
                    messagebox.showinfo("Fehler", "Fehler beim Erstellen der Output-CSV.")
                    return
                    
            if all_ceos_dict[ID]:
                for remaining_ceo in all_ceos_dict[ID]:
                    new_index = len(csv_dict)
                    csv_dict[new_index] = {}
                    for fieldname, value in enumerate(csv_dict[j]):
                        if value == "Geschäftsführer":
                            csv_dict[new_index][value] = remaining_ceo
                        else:
                            csv_dict[new_index][value] = csv_dict[j][value]
                    csv_dict[new_index]['Dualisbot'] = 1
        try:
            with open(file_path, 'w') as f:
                writer = csv.DictWriter(f, fieldnames=headers, delimiter=";")
                writer.writeheader()

                for i in range(len(csv_dict)-1):
                    row = {}
                    for j, fieldname in enumerate(csv_dict[i]):
                        row[headers[j]] = csv_dict[i][fieldname]
                    writer.writerow(row)

        except Exception:
            from dualisbot.gui.GUI import messagebox
            messagebox.showinfo("Fehler", "Fehler beim Schreiben der Output-CSV.")
            return

    def update_user_agents(self):
        return True
        file_path = Path(Config.UA_STORAGE_PATH)
        try:
            if exists(file_path):
                if os.stat(file_path).st_size == 0:
                    return True
                last_modified = getmtime(file_path)
                now = time.time()
                age_in_weeks = (now - last_modified)/(3600 * 24 * 7)
                if age_in_weeks > 2:
                    return True
                else:
                    return False
            else:
                return True
        except Exception as e:
            print(e)
            return True

    def save_user_agents(self, uas_json: dict):
        file_path = Path(Config.UA_STORAGE_PATH)
        try:
            with open(file_path, 'w') as f:
                for i in range(15):
                    f.write("%s\n" % uas_json[i]['useragent']) #todo: windows hat \r\n
        except Exception:
            pass

    def get_random_ua(self):
        file_path = Path(Config.UA_STORAGE_PATH, 'useragents.txt')
        try:
            with open(file_path) as f:
                uas = f.read().splitlines()

            return random.choice(uas)
        except:
            pass
