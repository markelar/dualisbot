import subprocess
import argparse
from twisted.internet import reactor
from scrapy.crawler import CrawlerProcess
from scrapy.settings import Settings
# from scrapy import log
from dualisbot.spiders.ceo_spider import CEOSpider
from dualisbot.spiders.ua_spider import UserAgentSpider
from dualisbot.spiders.ceo_result import CEO_result
from scrapy.utils.project import get_project_settings
from os.path import (exists, getmtime, abspath, join, dirname, isdir, realpath)
from pathlib import Path
import time
import pysnooper
from dualisbot.config import Config
from dualisbot.helpers import Helpers
from dualisbot.gui import GUI
# from dualisbot.gui import StartPage
# from scrapy.conf import settings
import sys
import msvcrt
import os
import json
import locale
from dualisbot.spiders.ceo_result import CEO_result


def load_config():
    configs = None
    try:
        config_path = join('dualisbot', 'config.json')
        with open(config_path, 'r') as f:
            configs = json.load(f)
    except Exception as e:
        print(e)
        pass

    if not configs:
        Config.UA_STORAGE_PATH = realpath(join("dualisbot","ua_headers.txt"))
        Config.OUTPUT_STORAGE_PATH = join(str(Path.home()), 'dualisbot')
        Config.SEARCH_TERMS_STORAGE_PATH = realpath(join("dualisbot","ceo_searchterms.txt"))

        try:
            if not isdir(Config.UA_STORAGE_PATH):
                os.makedirs(Config.UA_STORAGE_PATH)

            if not isdir(Config.OUTPUT_STORAGE_PATH):
                os.makedirs(Config.OUTPUT_STORAGE_PATH)

            if not isdir(Config.SEARCH_TERMS_STORAGE_PATH):
                os.makedirs(Config.SEARCH_TERMS_STORAGE_PATH)
        except Exception as e:
            pass
    else:
        Config.UA_STORAGE_PATH = configs['UA_STORAGE_PATH']
        Config.OUTPUT_STORAGE_PATH = configs['OUTPUT_STORAGE_PATH']
        Config.INPUT_STORAGE_PATH = configs['INPUT_STORAGE_PATH']
        Config.SEARCH_TERMS_STORAGE_PATH = configs['SEARCH_TERMS_STORAGE_PATH']

def save_config():
    config_path = join('dualisbot', 'config.json')
    with open(config_path, 'w') as f:
        configs = {
            'UA_STORAGE_PATH': Config.UA_STORAGE_PATH,
            'OUTPUT_STORAGE_PATH': Config.OUTPUT_STORAGE_PATH,
            'INPUT_STORAGE_PATH': Config.INPUT_STORAGE_PATH,
            'SEARCH_TERMS_STORAGE_PATH': Config.SEARCH_TERMS_STORAGE_PATH
        }
        json.dump(configs, f)

def run_bot():
    # if Helpers.update_user_agents(Helpers):
    #     process = CrawlerProcess(get_project_settings())
    #     process.crawl('UserAgent', domain="https://techblog.willshouse.com/2012/01/03/most-common-user-agents/")
    #     process.start()

    settings = get_project_settings()
    # header = {
    #     'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
    #     'Accept-Language': 'de-DE'
    # }
    # header['User-Agent'] = h.get_random_ua()
    # header['User-Agent'] = "Mozilla/5.0 (X11; U; Linux x86_64; de; rv:1.9.2.8) Gecko/20100723 Ubuntu/10.04 (lucid) Firefox/3.6.8"
    # settings.set('DEFAULT_REQUEST_HEADERS', header)

    if not Helpers.is_valid_path(Helpers, Config.INPUT_STORAGE_PATH, "Fehler beim Öffnen der Input-CSV-Datei."):
        return
    if not Helpers.is_valid_path(Helpers, Config.OUTPUT_STORAGE_PATH, "Bitte gültigen Output-Ordner angeben."):
        return
    csvdict = Helpers.csv_to_dict(Helpers)
    domains = Helpers.get_companies_url(Helpers, csvdict)
    if domains:
        process2 = CrawlerProcess(settings)
        for ID in domains:
            process2.crawl('CEO', domain=domains[ID], ID=ID)

        process2.start()
        Helpers.write_ceo_csv(Helpers)

if __name__ == '__main__':
    load_config()
    GUI.start_gui()
    save_config()
   